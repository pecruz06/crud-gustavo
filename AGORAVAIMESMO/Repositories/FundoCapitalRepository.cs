﻿using AGORAVAIMESMO.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AGORAVAIMESMO.Repositories
{
    public class FundoCapitalRepository : IFundoCapitalRepository
    {
        private readonly List<FundoCapital> _storage;

        public FundoCapitalRepository()
        {
            _storage = new List<FundoCapital>();
        }

        public void Adicionar(FundoCapital fundo)
        {
            _storage.Add(fundo);
        }

        public void Alterar(FundoCapital fundo)
        {
            var index = _storage.FindIndex(0, 1, x => x.id == fundo.id);
        }

        public IEnumerable<FundoCapital> ListarFundos()
        {
            return _storage;
        }
        public FundoCapital ObterPorId(int id)
        {
            return _storage.FirstOrDefault(x => x.id == id);
        }
        public void RemoverFundo(FundoCapital fundo)
        {
            _storage.Remove(fundo);
        }
    }
}
