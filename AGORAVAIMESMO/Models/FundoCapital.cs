﻿using AGORAVAIMESMO.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AGORAVAIMESMO.Controllers;
using AGORAVAIMESMO.Models;
using Newtonsoft.Json;

namespace AGORAVAIMESMO.Models
{
    public class FundoCapital
    {
        /*  public FundoCapital()
          {
              id = Guid.NewGuid();
          }*/

        [JsonProperty("Id")]
        public int id { get; set; }
        [JsonProperty("Nome")]
        public string Nome { get; set; }
        [JsonProperty("ValorNecessario")]
        public int ValorNecessario { get; set; }
        [JsonProperty("ValorAtual")]
        public int ValorAtual { get; set; }
        [JsonProperty("DataResgate")]
        public DateTime? DataResgate { get; set; }
    }

}
